var loopback = require('loopback');
module.exports = function(KasaUser) {
  KasaUser.newProduct = function(cb) {

    var userId = loopback.request.getUserId();
    cb({test:KasaUser});

  };
  KasaUser.remoteMethod(
    'newProduct',
    {
      http: {path: '/newProduct', verb: 'post'},
      returns: {arg: 'newProduct', type: 'object'}
    }
  );
};
